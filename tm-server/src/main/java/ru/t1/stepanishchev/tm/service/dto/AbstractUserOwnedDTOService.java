package ru.t1.stepanishchev.tm.service.dto;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.stepanishchev.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.stepanishchev.tm.dto.model.AbstractUserOwnedModelDTO;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedDTORepository<M>>
        extends AbstractDTOService<M, R>
        implements IUserOwnedDTOService<M> {

}