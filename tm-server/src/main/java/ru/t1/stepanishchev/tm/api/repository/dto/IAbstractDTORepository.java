package ru.t1.stepanishchev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;

public interface IAbstractDTORepository<M extends AbstractModelDTO> {

    void add(@NotNull M model);

    void addAll(@NotNull Collection<M> models);

    void update(@NotNull M model);

    void remove(@NotNull M model);

}